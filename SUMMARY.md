# :sunflower: Summary

* [Introduction](README.md)
* [Handbook](handbook.md)

### Practicalities
* [Airing](practicalities/airing.md)
* [Caring for indoor plants](practicalities/flowers.md)
* [Dish washing](practicalities/dishwashing.md)
* [Door and window handling](practicalities/doorwindowhandling.md)
* [Hot drinks](practicalities/hotdrinks.md)
* [Laundry](practicalities/laundry.md)
* [Saving Food](practicalities/savingfood.md)
* [Showering](rooms/k20-2-1-bathroom.md)

### Social Infrastructure
* [Coordination Meeting](social/come.md)
* [Power Hour](social/powerhour.md)
* [Core documents](social/documents.md)
* [Newcomer Induction](social/newbieinduction.md)
* [Positions and Evaluations](social/positionsandevaluations.md)
* [Roadmap plannings](social/roadmap.md)
* [MCM planning](social/MCM.md)

### Technical Infrastructure
* [Wi-Fi](technical/wifi.md)
* [Printing and Scanning](technical/printing.md)
* [House Bus](technical/housebus.md)
* [KeinMüllWagen](technical/keinmuellwagen.md)
* [Network](technical/network.md)

### Digital Infrastructure
* [GitLab](digital/gitlab.md)
  * [Residence record](digital/residencerecord.md)
* [Nextcloud](digital/nextcloud.md)
  * [Calendar](digital/calendar.md)
* [Local server](digital/server.md)
* [Email](digital/email.md)
* [Grafana](digital/grafana.md)

### Childcare
* [Diaper changing](childcare/diaperchanging.md)
* [Clothes](childcare/clothes.md)
* [Feeding](childcare/feeding.md)
* [Cooking baby food](childcare/cookingbabyfood.md)
* [Sleeping](childcare/sleeping.md)

### Rooms
* [Room numbering](rooms/roomnumbering.md)
* [K20-0-1: Washing Room](rooms/washingroom.md)
* [K20-0-2: Silent Office](rooms/silentoffice.md)
* [K20-1-1: Snack Kitchen](rooms/snackkitchen.md)
* [K20-1-2: Main Kitchen](rooms/kitchen.md)
* [K20-1-3: Dining Room](rooms/diningroom.md)
* [K20-2-1: Main Bathroom](rooms/mainbathroom.md)
* [K20-2-3: Communal Sleeping Room](rooms/communalsleeping.md)
* [K20-2-4: Communal Closet](rooms/communalcloset.md)
* [K22-1-4: Piano Room](rooms/pianoroom.md)

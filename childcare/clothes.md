# :shirt: Baby clothes

This article is about how to dress the child and where to find the clothes.

## Locations

All children's clothes currently in use are stored in the baby bathroom (K22-1-2). There are several containers with different pieces and purposes:
- The general clothes box in the top level of the shelf: Fresh clothes
- The body box in the middle of the shelf: Fresh bodies
- The tights box in the middle of the shelf: Fresh tights
- The used clothes box below the diaper changing table: Clothes that were worn already but can be reused
- The socks and hats bag below the diaper changing table: Fresh socks and hats
- The scarf bag below the diaper changing table: Fresh scarves in different sizes
- The 40°C laundry bag on the floor next to the door: Dirty clothes that should be washed
- The additional clothes bag behind the shelf on the edge of the bathtub: More fresh clothes in the current size or slightly bigger

If you happen to have some wet clothes, preferably put them to dry on the drying rack on the bathtub and then decide afterwards if you want to reuse them or put them to the washing.

## What should the child wear?

There are some general guidelines you can follow:
- The first layer over the diaper is always the body. (That's because it also has the purpose of holding the diaper in place.)
- In winter there should be always tights and socks on the child.
- Imagine yourself in the outfit of the child. Would you feel comfortable and warm enough? If yes, then it's probably good.
- If you think it might be too much clothing, touch the neck of the child: If it's sweaty it's too hot.
- Cold hands don't mean that the child is freezing, a cold belly though definitely does.

**Fresh or reuse?**
Well that's up to you to decide. We like to economize on laundry a lot, but if you feel that some of the reuse clothes are stinky, feel free to put them to the washing and take fresh things.

## Outside clothes

For going outside in winter we have a nice snowsuit. It's waterproof and thus great for flubbing around the playground...^_^
It's really warm, so you can remove the outer layer of Mika's clothes and just put him in the suit wearing a body, tights and a longsleeve (for example). To be able to walk outside he also needs his shoes, which are usually with the snowsuit or still in the buggy. If it's very cold or windy you should also put the fluffy white hat on his head below the hood of the snowsuit.

In short:
- long underwear (he already wears)
- snowsuit (either in baby bathroom or dining room)
- hat (either in baby bathroom or dining room)
- shoes (either in baby bathroom or dining room, or still in the buggy)

## Night outfits

To keep the bed clean it makes sense to change the child into a night outfit before putting them to bed. There is a hook next to the diaper changing table in the baby bathroom, on there the night outfit is stored. It is not necessarily a classic pajamas onezie, but can also be a combination of tights and soft pullover or whatever you decide should be the night outfit. Most important is that it's comfortable and  not too warm.

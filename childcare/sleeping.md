# :zzz: Sleeping

Putting children to sleep is a tricky task for all caretakers. Luckily there are some best practices.

## Preparation
- Make sure the child has an acceptably fresh diaper and is not hungry.
- There should be a baby phone in the free shop as well as in the sleep kitchen.
- There should be 'Mika sleeps here' signs in the free shop, in the sleep kitchen and in the communal sleeping room.

## Timing
You can stick to a schedule or just have a look at the child's behavior and judge for yourself if they're tired.

### Signs of sleepiness in a 1-year-old
- clumsy movement and frequent falling over followed by disproportionate unhappiness
- yawning and rubbing eyes
- overly hyper mood that can quickly change to crying

### Mika's rough sleeping schedule
- ~12h during the night
- getting up between 8 and 11 am
- one nap in the middle of the day, can be up to 4h even
- go to bed for the night between 7 and 10 pm

Of course the timing depends on previous sleeping periods. As a rule of thumb one can say that in the morning Mika is sleepy again quicker than in the evening. That means that sometimes he only gets up at 10:30 am and still starts his nap again at 1:30 pm. For putting him to bed in the evening he should have been awake around 4 hours before. But as always with living beings things can be different depending on the mood of the day, a potential sickness starting to be felt, more or less action and excitement, and whatever varying factors one can imagine.

## Putting Mika to sleep during the day
As of January 2020 we have the following ritual:
- Around noon Mika gets a fresh diaper, is put into his snowsuit, hat and shoes
- We take him in his buggy and go to the playground
- There he can walk around and play until he gets unhappy (usually ~15 minutes to an hour)
- We put him back into the buggy and drive him around until he falls asleep (usually less than 10 minutes)
- Then we park him in the free shop as he is and let him sleep there
- If it's very cold you can place another blanket on top of him
- Then just quietly leave the room and close the door
- There should be a baby phone in the free shop that just needs to be plugged in
- Take the receiver with you and turn it on by pressing the button
- Place the 'Careful, Mika is sleeping!' sign on the door to the free shop flat

You can also just go to the sleep kitchen with him and let him play there until he comes close again and lies down voluntarily. If he's sleepy he might just become cuddly and lie down right next to you on the big bed. Then you can spoon him until he's asleep. Make sure to wait until his breath is deep and regular before you get up, otherwise he'll be quickly up again.


## Putting Mika to sleep in the evening
As of January 2020 we have the following ritual:
- Around 8 pm Mika gets a fresh diaper and is put in his pajamas
- We brush his teeth (tips below)
- Then we take him to the sleep kitchen and put the sign on the door
- You can already plug in the baby phone and leave it in the small bed so that Mika won't be attracted by its' light
- Depending on his urge to move we put him in his sleeping bag now or a bit later (just make sure he doesn't get too cold!)
- We can still play with him a bit and when we think it's time we switch off the light and cuddle him gently
- Then we tell him in a gentle voice what his day was like until he's asleep
- We wait until his breath is deep and regular before we get up again, take the receiver of the baby phone and leave

The hardest part of this is to not fall asleep yourself...^^ If it's a bit late or you feel sleepy yourself to begin with you probably want to have brushed your teeth already and just stay in bed with him.

If you want to use the big bed in the sleep kitchen yourself with two people, it makes sense to transfer him somewhere else when he's asleep. We found it very convenient to not have him sleep in his own bed but rather on the extended couch next to the big bed, because then you can much easier put your hand on him to calm him down in case he makes sounds od distress during the night.

## The night's sleep
Mika sleeps very well alone in the sleep kitchen. If he makes sounds it does not necessarily mean that he's awake! Judge for yourself if action needs to be taken or if he will calm down again by himself. It's totally fine to wait a bit if he's not crying heartbreakingly.

If you think action should be taken go to the sleep kitchen quietly and simply give him some closeness, like a hand on his back. Better don't talk to him as he's most likely not really awake and you don't want to change that. You can slip into bed with him and spoon him a bit until his breath is calm again.

## Getting up
It seems to be good to let Mika some time to himself after waking up and not rush to his side the first time he makes a sound (if it's not crying). He seems to enjoy up to an hour of alone-time after waking up and is in a great mood afterwards - much better than when we drag him out when he's still not fully awake.

If you sleep in the same room as him it's also a good idea to spend some time together in bed in the morning until he's really awake and ready for the day. It's also a very nice and pleasant time to spend with him! :)

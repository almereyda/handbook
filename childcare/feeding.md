# :baby_bottle: Feeding

There are different kinds of food we feed to our children depending on how old they are and what time of day it is.


## Feeding a 1-year-old

After having completed their first year of life children can eat basically anything. Here's how we chose to feed ours.

### General tips & rough schedule
- If some preparations take too long, just give some snack food to the child already.
- If there's too much going on around the child might have a harder time focusing on eating.
- Some people prefer to feed the child without using the chair, do as you like.
- When you're done please remove excess food from the child's face, otherwise it will go everywhere.
- Janina always says 'hap' ([IPA](https://en.wiktionary.org/wiki/Appendix:German_pronunciation): /'hap'/) if she wants Mika to open his mouth, could be that he's already a bit conditioned to that word.
- The general feeding schedule says there are 4 meals a day, but this is also not carved in stone. If the child seems super hungry they can always have more! Same is true the other way around: If they obviously don't want to eat anymore, there's no need to try and force them.
- Mika eats approximately 1 jar of lunch food per day. Make sure to put a frozen one in the fridge before going to sleep, so that it's available the next day!

**Feeding**
- Put the child in their chair and attach the straps.
- Test the temperature of the food again with your own lip and start feeding if it's not too hot.
- If the child gets unhappy and tries to stand up, just wait a minute and see if they calm down again.
- Don't forget to offer water from the cup once in a while.

**Example feeding schedule**
- after waking up: breakfast
- around noon: lunch
- in the afternoon: 2nd lunch
- before going to sleep: dinner
- whenever in-between: snack food

### Breakfast & dinner

Breakfast usually consists of the main dish and a lot of snack food, because being around the breakfast table the child simply wants to participate in everything.

Dinner is basically the same food. We often prepare more of it in the evening so that we don't need to spend so much time on it in the morning when the child is usually more hungry.

#### Main dish: Oat porridge

**Preparation**
- First check if there's leftover oat porridge in the fridge, if yes use and/or extend it.
- Fill a plastic bowl with fine oats until its at most half full (otherwise it will boil over).
- Add water to generously cover the oats.
- Put it in the microwave for 2 minutes and watch it boil.
- Take out, stir, add some more water if necessary and put it back for another 1-2 minutes.
- Now it's really hot and needs to rest some 10 minutes (if it's winter you can put it outside on the windowsill).
- Stir again and add cold fruit sauce and water until consistency and taste are good.
- Add a small pile of milk powder if it fits the taste and stir it in.
- Add a vitamin D pill on top (once a day).

### Lunch 1 & 2

For lunch we use precooked baby food that you should find either in the fridge or in the freezer.
Running out of jars with food? [Here's the guide](cookingbabyfood.md) on how to produce the baby food in the first place.

#### Main dish: Veggie puree

**Preparation**
- There should be a (defrosted) jar in the fridge. Sometimes it's also a plastic box. In any case it's most probably labeled and on the bottom of the fridge.
- If it was frozen:
  - The consistency is more like foam and somehow weird and the puree needs to be heated up a lot to get a good texture again.
  - Also, the water separated a bit and should be partly removed. To do that push on the foamy veggie puree in the jar and drain the water that comes out.
- If it was not frozen:
  - The consistency of the puree is good it and just needs to be heated a bit.
- In any case refill half of the jar in a bowl and put it into the microwave.
- Once frozen food needs at least a whole minute, never frozen food only some 20 seconds.
- Take out, stir and add:
  - 1-2 tablespoons of rapeseed oil
  - 1-2 teaspoons of herbs
  - some lemon or lime juice
- Stir again, try the temperature and serve if it's not too hot.

### Snack food

Whenever the child seems hungry and/or unhappy for no apparent reason it can be bribed with food... ;)

**Is always okay**
- Bread
- Apple
- Pear
- Carrot
- Cooked potato
- Cooked veggies (most are no problem at all)
- Cucumber and bell pepper (although Mika doesn't really appreciate those)
- Some packaged snack food that you sometimes find on top or inside the blue Mika cupboard

**Is fine in moderation**
- Bread with spreads
- Sweet bread (like Hörnchen)
- Cooked beetroot (watch the stains... xD)
- Kaki
- Berries
- Mango
- Banana
- Melon

**Should be limited to a clear threshold**
- Not more than 1-2 pieces of mandarin or other citrus fruit
- Not more than some bites of seasoned adult food, like our communal dinner

**Should be avoided**
- Meat and fish (we want the child to stay vegetarian until they understand what they're doing)
- Highly processed food
- Very sweet or salty food

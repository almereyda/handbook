# :stew: Cooking baby food

This is a recipe for baby food that can be used from the time they are first starting to eat solid food until they have too many teeth to really care about puree anymore.

It is based on vegetables, potatoes and millet and thus vegan. We did not investigate deeply what a vegan diet needs to consist of for a baby, because ours always got milk and formula as well.

## Ingredients

- approx. 1/3 potatoes
- approx. 2/3 other vegetables
- approx. 10% millet
- if available add some apples

This is super vague because we never measure the amounts, sorry.

## Preparation

- Peel potatoes and roughly cut veggies.
- Fill the pressure cooker with potatoes and carrots.
- Put the other veggies, the millet and the apples in another, even bigger pot.
- Add water and boil until soft.
- Remove excess water from both pots.
- Mash the potatoes and carrots (do _not_ blend them!)
- Blend the veggie-millet-mix.
- Mix both things together.

## Bottling

- Get a decent amount of big 77 jars from the snack kitchen (at least 15).
- Make sure to use the lids that can seal (they have an elevated circle in the middle).
- No need to sterilize them, just have them ready when the food is prepared and still hot.
- Fill each jar to 2/3 of its height and immediately close (using the jam funnel helps a lot).
- Get the labels from the blue Mika cupboard in the dining room and apply one to each jar.
- Put the filled jars into a box and let them cool down somewhere.

## Storing

- Some jars will have sealed during the cool-down and some won't.
- Freeze the jars that did not seal.
- Put the jars that did seal in the fridge.

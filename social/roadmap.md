# :paw_prints: Roadmap plannings

__ARCHIVED! This was valid from 2017 to 2019.__<br>
__In September 2019 roadmap plannings were changed to [MCM](MCM.md).__

We have quarterly roadmap meetings, in which we decide on what we want to do in the next three months.

### Scope

Roadmap tasks are more complex tasks, which need more time and also yield more sustainable results than short-term actions. This is why every roadmap is also about deciding where we want the project to go over the coming months.

### Seasons

Our year is organized in 4 seasons:
 - Winter (December to February)
 - Spring (March to May)
 - Summer (June to August)
 - Autumn (September to November)

### Meetings

In the beginning of a season we have the seasonal roadmap planning meeting (e.g. Spring roadmap meeting). In the middle of a season a roadmap goal awareness meeting can make sense, so that the tasks are really done and not forgotten. In the end of a season a roadmap retrospective meeting can be arranged to talk about what was actually done, which goals were missed, why this happened and what to learn from it.

We also discovered that a dreaming session in between two roadmaps is very useful. This way we can take the time to clarify our bigger visions again and then derive clear tasks in the roadmap planning meeting. Without a dedicated time to share dreams, the roadmap planning meeting might get unfocused and lengthy because people's ideas are not aligned enough to come up with concrete things to do.

On the verge of a season the schedule might look like this:
- 1st day: Last Roadmap Review/Retrospective
- 2nd day: Dreaming Session (e.g. about room layout, the bigger vision of the house, what to do with roofs, basements and garden, who we as a project want to be or general dreaming in the context of the house)
- 3rd day: Upcoming Roadmap Planning (like [this one for Autumn 2018](https://kanthaus.online/governance/minutes/2018-09-04_roadmap))

### Structure

The general structure of the roadmap planning was inspired by [the software project Karrot's roadmaps](https://github.com/yunity/karrot-frontend/blob/master/ROADMAP.md) and is still not perfectly adapted to the physical project that is Kanthaus.

However, we do follow a sequential order that organically grew over the past roadmap meetings:
1. A pad is created that serves as the base for the meeting, as well as its minutes.
1. Everyone estimates how much time they will spend in Kanthaus over the next 3 months and writes it in the pad.
1. Everyone estimates how much time they will spend on roadmap tasks over the next 3 months and writes it in the pad.
1. Everyone lowers their estimations once more so that we don't take on too much.
1. A time frame is agreed upon in which people separate to think about tasks on their own. There are two categories:
  1. Tasks we want to personally commit to
  1. Tasks we want to be done, but which we can't/won't do ourselves
1. We come back together and in a round we collect everyone's ideas in the pad.
1. We order the pad and make it ready to be published.
1. We create issues on gitlab for the tasks we personally commit to and assign ourselves.
1. (Optional) We link our issues in the pad to make the points clickable in the minutes.

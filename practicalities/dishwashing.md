# :droplet: Dishwashing

The dishwasher uses way less water and space than manual cleaning, so please use it for most dishes. Exceptions are listed below:

### What not to put in
- Don't put wooden things in the dishwasher! The wood will absorb the hot water, swell and break.
- Don't put sharp knives in the dishwasher!
- Don't put fragile glasses in the dishwasher! They will collide and break.
- Don't put big items in the dishwasher! Pots, pans and bowls are better cleaned by hand as they take up too much space in the machine.

### How to put things in
- There are rotating devices inside the dishwasher, which are necessary to evenly distribute the water, which in the end cleans everything. If rotation is blocked, narrow items are placed on the far ends of the cleaning area, or the filter down below is full of dirt, there is no way that cleanliness can be achieved.

### How to put things out
- You can either remove the items one by one or take the whole tiers out, carry them to the [kitchen](/rooms/kitchen.md), empty them and put them back.

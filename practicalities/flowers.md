# :tulip: Caring for flowers

There are a few plants silently living with us. They need the right amount of love to be comfortable.

## Orchids (_Phalaenopsis_)

[Phalaenopsis](https://en.wikipedia.org/wiki/Phalaenopsis) looks like this:

<img src="https://www.ikea.com/PIAimages/0187960_PE340924_S5.JPG" alt="Image of Phalaenopsis orchid" height="400"/>

There are a few of them in the house. They're epiphytic: in nature they live on trees. They have aerial roots.

### Care

- Don't overwater them! When the substrate dries (about once a week) run water through the substrate and wait until it goes out through holes at the bottom. Don't let the roots stay in water and don't put water over the leaves. Otherwise they will rot and the orchid will die. Too much water is more dangerous for them than too little.
- Keep them in a light place, but away from direct sunlight. Too much sun: leaves turn reddish. Too little sun: leaves turn dark green and the plant won't flower.
- When the flowering is over, cut the flower stem away in the middle. It might reflower.

More details can be found on [WikiHow](https://www.wikihow.com/Care-for-Phalenopsis-Orchids-(Moth-Orchids)) and [beautifulorchids](http://www.beautifulorchids.com/orchids/orchids_potted/phalaenopsis/phalaenopsis_care.html)

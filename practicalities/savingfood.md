# :pear: Saving Food

Almost all the food we eat is saved. Here you can find out how that works.

### Getting the food

The first step is, of course, to get the food into the house. We have two main ways of doing so:

1. foodsharing Pickups

Officially agreed-on cooperations with stores are the best way to save food. We already have some cooperations and assign the task to carry them out every week in the [Coordination Meeting](/social/come.md).

2. Dumpster-diving

We also save food from dumpsters of supermarkets. We have several good spots we visit either by bike or by train normally. If you want to see the true extent of food waste for yourself, you should really join in and take a look into a dumpster!

When you come back from a successful food haul, you'll have crates and bags full of very ripe fruit, partly foul veggies and potentially soiled packaged food. Put those crates and bags on the floor of the [washing room](/rooms/washingroom.md) - no need to clean everything late at night. Very possible that someone else takes over the next morning.

### Cleaning and sorting the food

Everybody will be thankful for people who clean and sort food! Here's how you go about it:
**Cleaning**
- Everything that was dumpster-dived needs to feel some water!
- The best way to wet-clean food is a system using two buckets full of water: The first one to roughly clean it and the second one to remove the rest. If some items are too dirty you can clean them using running water, of course, but please don't be wasteful!
- When using running water, make sure there is a big bucket well-placed underneath the elevated bathtub to catch the used water. The more water you use the further the water coming out of the drain will splash, take that into account when placing the bucket! (And please, if you flood the washing room, make it dry again - there is a room full of dirty laundry right next to you... :wink:)
- 1-2 green boxes fit on the side of the bathtub. That's where you can leave cleaned food to dry. If you have more food than fits there you can either put another box on a dirty towel on the table next to the toilet or just continue later.
**Sorting**
- We have several locations to store food, which may change depending on the season. Please watch out for signs in the house to know the relevant locations for different food items!

(tbc)

# :calendar: Calendar
We host our own (CalDAV) calendar on our Nextcloud instance: https://cloud.kanthaus.online/apps/calendar

## Calendars overview
- **main** - Kanthaus events of relevance to most residents
- **main-long** - as for main, except only if the event spans multiple days (this calendar exists not to crowd the main calendar, which is an issue for phone users.)
- **regular** - repeating Kanthaus events which don't affect everyone
- **people** - for recording visitor visiting times
- **residents** - for residents to indicate absence/presence as they wish
- Deprecated? **wurzen** - events happening in Wurzen
- Deprecated? **leipzig** - events happening in Leipzig. See [trichter.cc](https://trichter.cc/) for more
- **müll** - waste collection dates from [KELL](https://kell-gmbh.de/service/pdf-kalender/)
- **festivals-events-etc** - for external events that may be of interest to others
- **KMV** - if you're planning on taking the transporter away from Kanthaus, record that here
- **Contact birthdays** - automagically generated from birthdates in the 'khontacts' contact book

## Android/AOSP phone access
- Download [Davx5](https://www.davx5.com/)
    - Recommended to download via [F-droid](https://f-droid.org/packages/at.bitfire.davdroid/)
- Open Davx5, add new account, select "Login with URL and user name",
    - Base URL: `https://cloud.kanthaus.online`
    - User name: <yourNextcloudUserName>
    - Password: <yourNextcloudPassword>
- Got to 'Caldav', select all calendars you want to sync

## Laptop/computers
- Your calendar app _should_ already have Caldav capabilities
- Find 'add calendar' or equivalent
- Use the same details as for Android/AOSP

# :memo: Residence record

In the residence record, which is a file in [the private kanthaus repository on gitlab](gitlab.md), we record the residents of Kanthaus.

### Why are people recorded?
- Because of our [positions system](positionsandevaluations.md), which requires us to know how much time individuals spend at the space.
- We love data. That's why we collect a lot of that, also on people.

### How and when are people recorded?
- Whoever spent a night in Kanthaus will be recorded for the following day.
- People are listed chronologically. That means if Lisa and Bob arrive on the same day, but Bob comes at 5pm and Lisa at 8pm, Bob is recorded before Lisa.
- Many names are quite common, so we add a suffix. Whoever records a person first can decide on the suffix and if it is even needed. Common suffixation patterns are: first letters of last name or abbreviation of context organization.
- If you record someone with a frequent name make sure there has not been someone else recorded without a suffix already!
- Adding suffixes to past records is completely fine. What counts is reliable data.
- The residence record ideally is updated every day to avoid making mistakes.
- Funny and/or interesting commit messages for the residence update are highly appreciated! Talk to Nick for inspiration.

### What if I don't want to be recorded?
- You can provide us with a consistent pseudonym, that we will use instead of your name.
- But keep in mind: it's a _private_ file anyways and we never record full names.

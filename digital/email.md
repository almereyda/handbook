# Email accounts

We have email accounts hosted on [Uberspace.de](https://uberspace.de/en/).

## Creating a personal account

Login to Uberspace's dashboard with Kanthaus' account and go to [the Email tab](https://dashboard.uberspace.de/dashboard/mail).
At the bottom of the page you will find "Virtual Mailboxes" section where you can add your own. Pick a fresh password for it.

You can access your account via the webmail at https://webmail.uberspace.de/

You can also configure the email account in your email client, with the following settings (which can generally be auto-detected):

* Server (both for IMAP and SMTP): gehrels.uberspace.de
* Connection security: STARTTLS
* IMAP port: 143
* SMTP port: 587

## Getting access to Kanthaus email

Once you have created a personal account, you can set it up so that it contains the general email received by Kanthaus (at the `hello` address)
as a subfolder. You can set it up as follows:

* SSH into `gethrels.uberspace.de` (username `kanthaus`). This can be done by password or SSH key.
* Navigate to your email folder (`cd users/firstname` where `firstname` is you email user name)
* Create a symlink to the `hello` account with `ln -s ../hello/ .hello`
* Add the `hello` folder to your subscriptions: `echo hello >> subscriptions`

Once that is done you should have access to Kanthaus email in your email client.

When replying to Kanthaus email we recommend you leave the `hello` address in Cc: and Reply-To:, so that further replies are visible for others.


# Grafana server

We use a [Grafana](https://grafana.com/) instance to monitor various things in the house, which can be accessed at https://grafana.yunity.org/.

## Getting access

You need a [GitHub](https://github.com) account and need to be part of the [yunity organization](https://github.com/yunity).

## Logging new quantities in Grafana

To track new metrics in Grafana you will need to store them in the underlying Prometheus database. TODO document the process

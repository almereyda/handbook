# :signal_strength: Wi-Fi

## simply and quick
- Network/SSID: `kanthaus-gast`
- no password

This network offers you about 8 Mbps download and 2 Mbps upload. Your traffic will be routed through a Dutch VPN provider. Please make sure that you encrypt your traffic yourself appropriately or do not consider this network to be secure.

## more secure, faster, printing, ...
- Network/SSID: `kanthaus`
- CA-Certificate:  [PEM (Linux, Android,...)](/assets/kanthaus_ca.pem) or [PKCS12](/assets/kanthaus_ca.p12)

#### Usernames
The usage of Multiple usernames allows us to serve different types of access. Grab the one that suits you!

| Username | Description | Examples |
| --------- | --------- | ---------------------- | 
| `normal`   | For all who simply need the Internet and maybe want to print something. | Smartphones, Tablets, Laptops of normal users |
| `full`     | For all who need full access to the network and want to see other devices | LAN party     |
| <code style="white-space: nowrap">full-temp</code> | When people temporarily need full access to the network | LAN Party with guests | 
| `vpn`      | Routed through the netherlands. Same like `kanthaus-gast`, but with encrypted Wi-Fi. (passwort is `vpn`) | Neighbors and Travellers on the street with more safety awareness |
| `admin` | Full access to all networking devices, including web interfaces of routers, switches and firewall | Network admins |

#### Installation: Linux / Networkmanager
Select the network `kanthaus` and click on "Connect"<br />
<img src="/images/linux_select_network.png" style="width:400px" />

Set following settings
1. `Tunneled TLS`
2. Download and select the [CA Certificate](/assets/kanthaus_ca.pem) (maybe you need to right click -> save as)
3. `MSCHAPv2`
4. Select a username from the username table above (e.g. `normal`)
5. Password for all users you get by asking in the kanthaus

<img src="/images/linux_credentials.png" style="width:400px" /><br />


#### Installation: Android 9
- Download the [CA Certificate](/assets/kanthaus_ca.pem) and save it somewhere on your phone
- Settings -> Security & location -> Encryption & credentials -> Install from SD Card -> select the downloaded certificate
- Use a name like "Kanthaus" and select "Wi-Fi" as the crential use
  <br /><img src="/images/android8_name_certificate.jpg" style="width:400px" />
- Connect to `kanthaus`, select the CA certificate and enter the credentials
  <br /><img src="/images/android8_enter_credentials.jpg" style="width:400px" />

### Installation: Android 4.4
to be written...
(simply never connect without the CA Certificate!)

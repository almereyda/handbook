# Network setup

This page should document some bits of the network infrastructure of kanthaus. Please maintain so people with a bit of network knowledge can understand and maintain the network.

## Overview
* Telekom VDSL 100 internet connection
* FritzBox DSL router used for Dect/VOIP
* different sub-networks spread via VLAN on the cable network
* one Archer C5 for DHCP and routing/NAT/Firewall different sub networks for guest/printer/etc.
* a bunch of TPLink Archer C5 running a specially patched version of  OpenWRT for Wifi (ath10k AP VLAN mode, should be available upstream with the next release; unavailable as of 19.07.3)

## Devices

| Device | Name | IP-Address | Location | Description |
| -------| -----| -----------| --------- | -------|
| FritzBox 7530 | fritz.box | 192.168.178.1 | K20-1 | DSL termination (on main network), telephone |
| Archer C5 | dragon-k20-1 | 192.168.178.2 | K20-1 | DHCP, Firewall, freeradius, routes restricted + IOT + printer subnetworks, extracts printer VLAN for Xerox printer |
| Server, i5-2500K, 16GB Ram | kanthaus-server | 192.168.178.31 | Silent Office | VPN termination & routing, file storage, cloud mirror, foodsharing gitlab CI Server, housebus logging & time/sunset provider |
| Archer C5 | dragon-k20-outside | 192.168.178.4 | Outside of silent office | Wifi Access Point |
| Archer C5 | dragon-k22-1 | 192.168.178.3 | Piano Room | Wifi Access Point |
| WDR4300 | dragon-k22-1 | 192.168.178.5 | K20-0 hallway | "Switch" |
| Archer C5 | dragon-k22-2 | 192.168.178.6 | K22-2 hallway | Wifi Access Point |
| TL-SG108E v3 | | 192.168.178.9 | K20-B Electric room | Managed Switch |
| GS108E v3 | | 192.168.178.13 | K20-1 hallway | Managed Switch |
| GS108E v3 | | 192.168.178.10 | K22-B entrance area | Managed Switch |

## Networks

### "Full"
* IPv4 192.168.178.0/24
* IPv6 by fritz box
* available untagged on all non-special ethernet ports
* bridged to wuppdays wifi (legacy; tbr) as well as kanthaus "resident" wifi
* DHCP (IPv4) by Archer in K20-1, sends out main fritz box as gateway (to not having to route heavy traffic through the archer)

### "Restricted"
* 192.168.2.0/24 IPv4 only
* available as VLAN100 on all non-special ethernet ports
* bridged to kanthaus "restricted" wifi
* DHCP by Archer in K20-1
* Routing by that archer as well; SQM QoS limiting to 90/35 Mbit/s
* Only access to internet + printers

### "VPN"
* 192.168.3.0/24 IPv4 only
* available as VLAN101 on all non-special ethernet ports
* bridged to kanthaus-gast as well as kanthaus "vpn" wifi
* only access to internet via VPN
* DHCP by Archer in K20-1 hands out server IP 192.168.3.2 as gateway
* VPN termination on kanthaus server; VPN throughput should reach nearly 100/40 Mbit/s

### "Printer"

* 192.168.4.0 
* VLAN 4, untagged marked port on K20-1 switch

### "SMA"
* 192.168.5.0/24 IPv4 only
* available as VLAN 5 between main archer, K20-1 and K20-B; available as untagged on two ports of the switch in K20-b next to solar inverters
* only access to internet
* kanthaus-server has an interface in here as well to log solar information into influxdb

## Topology
* DSL coming from K20-B to K20-1; fritz box is there so it offers DECT coverage in all rooms of both houses + garden.
* K20-1: Main Archer access pgoint next to fritz box; network cables to K20-1 and K20-B
* K20-0: AP as switch distributing cables just in K20-0
* K20-B: Managed 8-port switch with IGMP Snooping and VLAN handling for solar inverters; connects K20(-2) and K22(-B)
* K22-B: Dumb 5-port switch and cables into each of the flat of K22

## Pitfalls
* We use 802.1q VLAN tagging. All switches everywhere need to have at least passive passthrough support, otherwise the Vlans disappear at that switch. I don't know of any gigabit switch that does not support this.
* Again Vlan: Managed switches normally need to have all VLANs that should be forwarded (also tagged -> tagged forward) defined in them.
* Again Vlan: Some Access Points like WDR841 v7/v8 cannot handle tagged and untagged vlan on the same port at the same time. So far, we don't have any equipment like that and likely we will never have, but just to know...
* Again Vlan: fritz boxes have their switch in managed vlan mode and don't forward any tagged vlan

# :couple: Communal Sleeping Room (or K20-2-3)

The communal sleeping room is a communal relaxation area with no personalized beds.

### Facilities

- Mattress space that spans the whole room length from one wall to another (1x small and 2x big mattresses)
- One individual mattress behind the door
- One cot behind one of the big mattresses
- A projector that can is mounted to project to the ceiling
- A sound system

### Room availability

Who needs a bed should find one here. Apart from sleeping the room can also be used for–
* silent talking
* calm reading sessions
* watching movies (from time to time)
* (more or less intense) cuddling

You are not guaranteed to have the same bed/place every night, so __please remove all your personal belongings__ when you get up.

The room is not intended for storage, not even for your clothes during the night. Please use the [communal wardrobe room](communalcloset.md) to get into your sleeping clothes to keep noise and clutter out of this room.

### Specials

As the room is supposed to take up to 10 people, sleeping here means you might be quite close to the people around you. Regular use of this room usually does not exceed 6 people though.
You should not be annoyed by others being a bit noisy, but still the main purpose of this room is sleeping and relaxation.  
Who goes up first can choose their blanket, pillow and mattress freely.  
You may see quite a lot of skin in this room, this should not upset you.

### Cinema

This room can and should be used for watching movies in a nice, cozy atmosphere.
Before watching a movie late in the evening, communicate to the others when you are about to start and how long it might take, so people can arrange other sleeping possibilities if they might be disturbed by it.

![Projector/sound setup](/images/communal-sleeping-kino.jpg)

The projector can be attached via HDMI or Mini-Displayport.
An audio cable is attached to the projector so that you only need a single connection from your device.

Volume can be adjusted with the top knob at the subwoofer next to the projector.

Please plug/unplug the system before/after you use it at the plug near the pillows.

If you want to watch movies during the day you can use the blinds which are installed on the windows to shut out most of the daylight.

# :stew: Main Kitchen (or K20-1-2)

The main kitchen is a room for preparing and saving (communal) meals.

### Facilities (Needs to be updated!)
- dishes
  - ~25 big soup bowls (deep plates)
  - ~15 big plates
  - ~40 small plates
  - ~30 sets of cutlery
  - 4 big cooking knives
  - 5 smaller cutting/peeling knives
- cooking utilities
  - 2 huge pots (8-10 liters)
  - 2 big pots (5 liters)
  - many smaller pots
  - medium-sized wok
  - 3 pans (~30cm)
  - enough plastic, metal and wood cooking cutlery
- Gas cooker with 2x 3.5 kW flames
- household oven (sorry, only top/down heat, convection broken. Want to fix it?) with 3 baking trays
- 2 ceramic stove tops (2 kW each)
- utility devices
  - 1.7 liter 2kW cattle/water cooker
  - household kitchen machine with blender, juice squeezer, cutting helper
  - handmixer
  - 5 kg scale (fine 125g scale is in the workshop)
  - 3.5 liter tea thermos flask
  - 3x 1 liter thermos flasks
- When entering the room from the hallway, one can find the **used dishes shelf** on the left. People can claim compartments here to reuse dishes and/or cutlery.
  - In the middle of the shelf you can find empty teapots. Feel free to [fill them up](/practicalities/hotdrinks.md) with the drink of your choice if there are no or not enough tea already available.
- When taking two steps ahead you'll find the **tea cupboard** on your left.
  - On top of the cupboard there is a multi-pole socket, an audio cable, that leads to an amplifier and lets you play music on speakers (more info below) and a landline phone that also has a charging micro usb cable for mobiles.
  - Next to the socket you find the **full teapots**. Serve yourself!
  - Above the full teapots there is the **wooden board for teabags**, where a selection of teabags can be found.
    - On top of this wooden board there is an electronic piece, which actually is an **amplifier for our kitchen's sound system**. The volume can be regulated with a control dial on this bit.
  - Further down there is a huge compartment full of tea, coffee and other ingredients for hot beverages.
  - Below that there's two doors: Behind one you'll find tea towels and sponges and behind the other there's baking paper, rubbish bags and other practicalities.
- When looking ahead you can see the **cupboards and shelves for dishes**. There are also some compartments for unsealed jars of food. Both should be self-explanatory.
- Turning right you'll find the **kitchen table and some chairs**. These spots can be used for various purposes, but if someone wants to cook, you should not be in their way.
  - Above the table there's a small shelf for open sauces and spreads
- Behind the table there's the **food cabins**.
  - The wall cupboard holds breakfast and baking ingredients, like cereals and flour.
  - The cupboard on the floor has pasta, rice and similar things in the upper compartment and a **special compartment for bought food** below. Further down there's refills for our spices.
  - The space between the two should be mostly free but is also used for our **kombucha**.
- In the corner there's our **gas stove**. It's operated with a gas bottle located behind the stove and offers the highest cooking speed due to the biggest flames.
  - Below the stove the pots are stored.
- On both sides of the window there are **spices and oil shelves**.
- On the right side of the window you'll see a big countertop which incorporates the main oven. On the rightmost edge of it there's an **electric kettle** and a **kitchen machine**.
- The **main oven** and the corresponding stove are also operated by bottled gas - you can see the gas bottle next to the oven. Still, no lighter is needed is start cooking or baking: Just plug in the oven and turn the knobs and everything should work! If not, the gas bottle may be closed or empty.
- On the right of the oven there is the sink. It has cold, drinkable water and a drying area on the left. Items that are too big, too sharp, too fragile or too wooden to be put into the [dishwasher](/practicalities/dishwashing.md) should be cleaned here. Clean, wet items are to be placed on the left of the sink, while items which still need cleaning should be put to the right - or, even better, immediately rinsed.
- Below the sink you'll find our **recycling station**. A multitude of buckets holds glass, rest waste, kompost, organic waste, paper and plastics. A sign above the sink explains this system in more detail.

### Room availability

This room is a public one. It should be most accessible for people who want to prepare meals for a bigger group.

### Specials

When cooking a lot of moisture is released. Please make sure to properly ventilate the room, especially in winter! [Proper ventilation](/practicalities/airing.md) means to keep the window open a lot of the time during evaporation-intense procedures in the kitchen, and to open the windows on the other side of the building as well to enable wind going through. In the case of the kitchen this would mean to open the door to the [dining room](/rooms/diningroom.md) and to open the windows there as well.
